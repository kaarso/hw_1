import java.util.Arrays;

public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
      // for debugging

      ColorSort.Color[] balls = null;

      int rCount = 0;
      int gCount = 0;
      int bCount = 0;

      balls = new ColorSort.Color [20];
      rCount = 0;
      gCount = 0;
      bCount = 0;
      for (int i=0; i < balls.length; i++) {
         double rnd = Math.random();
         if (rnd < 1./3.) {
            balls[i] = ColorSort.Color.red;
            rCount++;
         } else  if (rnd > 2./3.) {
            balls[i] = ColorSort.Color.blue;
            bCount++;
         } else {
            balls[i] = ColorSort.Color.green;
            gCount++;
         }
      }
      System.out.println(Arrays.toString(balls));
      ColorSort.reorder (balls);

   }
   
   public static void reorder (Color[] balls) {
       // TODO!!! Your program here
       Color[] sortedBalls = new Color[balls.length];
       int counter = 0;

       for (int i = 0; i < balls.length; i++) {
           if (balls[i] == Color.red) {
               sortedBalls[counter] = balls[i];
               counter++;
           }
       }
       for (int i = 0; i < balls.length; i++) {
           if (balls[i] == Color.green) {
               sortedBalls[counter] = balls[i];
               counter++;
           }
       }
       for (int i = 0; i < balls.length; i++) {
           if (balls[i] == Color.blue) {
               sortedBalls[counter] = balls[i];
               counter++;
           }
       }
        // system.arraycopy viide: https://www.programcreek.com/2015/03/system-arraycopy-vs-arrays-copyof-in-java/
       System.arraycopy(sortedBalls, 0,balls,0,balls.length);

   }
}

